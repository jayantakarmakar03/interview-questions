/****************20+ Array Questions from Programming Interviews********************************************************************
Q> How do you find the missing number in a given integer array of 1 to 100? (solution)____[DONE]____
Q> How do you find the duplicate number on a given integer array? (solution)____[DONE]____
Q> How do you find the largest and smallest number in an unsorted integer array? ____[DONE]____
Q> How do you find all pairs of an integer array whose sum is equal to a given number? (solution)
Q> How do you find duplicate numbers in an array if it contains multiple duplicates? (solution)
Q> How to remove duplicates from a given array in Java? (solution)____[DONE]____
Q> How do you search a target value in a rotated array? (solution)
Q> Given an unsorted array of integers, find the length of the longest consecutive elements sequence? (solution)
Q> How is an integer array sorted in place using the quicksort algorithm? (solution)
Q> How do you remove duplicates from an array in place? (solution)
Q> How do you reverse an array in place in Java? (solution)
Q> How are duplicates removed from an array without using any library? (solution)
Q> How to convert a byte array to String? (solution)
Q> What is the difference between an array and a linked list? (answer)
Q> How do you perform a binary search in a given array? (solution)
Q> How to find a median of two sorts arrays? (solution)
Q> How to rotate an array left and right by a given number K? (solution)
Q> How do you find duplicates from an unsorted array? (solution)
Q> Given an array of integers sorted in ascending order, find the starting and ending position of a given value? (solution)
Q> Given an integer array, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum?____[DONE]____
Q> Find if there is a subarray with 0 sum? (solution)   ____[DONE]____
    Input: [4, 2, -3, 1, 6]
    Output: true

-----How to solve String-based Coding Problems-----------
----------------------------------
How do you reverse a given string in place? (solution)
How do you print duplicate characters from a string? (solution)
How do you check if two strings are anagrams of each other? (solution)
How do you find all the permutations of a string? (solution)
How can a given string be reversed using recursion? (solution)
How do you check if a string contains only digits? (solution)
How do you find duplicate characters in a given string? (solution)
How do you count a number of vowels and consonants in a given string? (solution)
How do you count the occurrence of a given character in a string? (solution)
How do you print the first non-repeated character from a string? (solution)
How do you convert a given String into int like the atoi()? (solution)


How do you reverse words in a given sentence without using any library method? (solution)
How do you check if two strings are a rotation of each other? (solution)
How do you check if a given string is a palindrome? (solution)
How do you find the length of the longest substring without repeating characters? (solution)
Given string str, How do you find the longest palindromic substring in str? (solution)
How to convert a byte array to String? (solution)
how to remove the duplicate character from String? (solution)
How to find the maximum occurring character in given String? (solution)
How do you remove a given character from String? (solution)
Given an array of strings, find the most frequent word in a given array, I mean, the string that appears the most in the array. In the case of a tie, ​the string that is the smallest (lexicographically) ​is printed. (solution)
************************************************************************************/
/* all array sybarray related questions
Q.> Find Maximum Subarray Sum: Given an array of integers, find the contiguous subarray with the largest sum.
Q.> Subarray with Given Sum: Given an array of positive integers and a target sum, find if there exists a subarray with the given sum.
Q.> Longest Increasing Subarray: Find the longest increasing subarray within an array of integers.
Q.> Count Subarrays with Given Sum: Count the number of subarrays within an array of integers that sum up to a given value.
Q.> Smallest Subarray with Sum Greater than X: Find the length of the smallest subarray with a sum greater than a given value.
Q.> Subarray Product Less Than K: Given an array of positive integers and an integer K, count the number of contiguous subarrays 
    where the product of all the elements is less than K.
Q.> Subarray Division: Given an array of integers and a target sum, find the number of ways to divide the array into subarrays such that
     each subarray has a sum equal to the target sum.
Q.> Longest Subarray with Equal 0s and 1s: Find the length of the longest subarray within an array of 0s and 1s such that the number of 0s and 1s are equal.
Q.> Maximum Length of Subarray with Equal 0s and 1s: Find the maximum length of a subarray within an array of 0s and 1s where the number of 0s and 1s are equal.
Q.> Subarray Sum Equals K: Given an array of integers and an integer K, find the total number of continuous subarrays whose sum equals K.
Q.> Subarray Sum Divisible by K: Given an array of integers and an integer K, find the total number of continuous subarrays whose sum is divisible by K.
Q.> Maximum Length of Subarray with Sum Zero: Find the maximum length of a subarray within an array of integers whose sum is zero.
Q.> Maximum Average Subarray: Given an array of integers, find the contiguous subarray with the maximum average.
Q.> Subarray Sort to Make Array Sorted: Find the shortest subarray that, when sorted, makes the whole array sorted.
Q.> Longest Subarray with Equal Elements: Find the length of the longest subarray within an array where all elements are the same.
***************************************************************************************************************************/
/******************************************************************************************************************************************************
 ----------Things Every Programmer should know about Array------------------------------
Array index starts at 0, not 1.
Array are mostly immutable data structure whose length cannot be changed once created, the mutable array is called list.
The array needs a memory block for allocation, called consecutive memory location, this means even if you have a memory you cannot allocate a big array if memory is scattered.
Searching by index in the array is O(1) but insert and delete is not easy because you may need to re-arrange the array.
An array is mostly homogenous data structure this means you cannot store a string in an integer array and vice-versa.
An array can be single dimension or multiple dimension. A two-dimensional array is known as Matrix and very useful in games to create 2D world using tiles.
 ***********************************************************************************************************************************************************************8*/
try{
    //####################################################################################################################################
    //1. ##JavaScript, How do you find the missing number in a given integer array of 1 to 12
    //####################################################################################################################################
  (function missingnum(){   //immediate invoke function 
      let array = [1,2,5,7,9,12],
      count = 12,
      missing = new Array(),
      missing2 = new Array();

      //using indexof method [1st approach]
      for (let i = 1; i <= count; i++) {
        if (array.indexOf(i) == -1) {
          missing.push(i);
        }
      }
//----------------------------------------------------------------------------------------------------------------------------

      // using include method  [2nd approach]
      for (let i =1; i<=count;i++){
         !array.includes(i) && missing2.push(i)  //if array.include==false means number is not present in the array
      }
      console.log(`Missing numbers from array are : ${missing}`); // to check the result
      console.log(`2nd method Missing numbers from array are  : ${missing2}`); // to check the result

     // output 
     //Missing numbers from array are : 3,4,6,8,10,11
     // 2nd method Missing numbers from array are  : 3,4,6,8,10,11
  })()
      

  //####################################################################################################################################
  //2. ##JavaScript, How do you find the duplicate number on a given integer array?
  //####################################################################################################################################
    //##### 1st Approach 
    //using filter() and indexof ()
        var array = [1,2,3,3,4,5,5,6,7,7,8,9,9,10,11,11,12,13,13,14,14,14,14]; 
        function findDuplicates(arr) {
          // syntax of filter : array.filter((currentValue, index, arr)=>this value))
          return arr.filter((currentValue, currentIndex) => arr.indexOf(currentValue) !== currentIndex);
          // find unique elemnt in array  only 
          // return arr.filter((currentValue, currentIndex) => arr.indexOf(currentValue) == currentIndex);
        }
        let data = findDuplicates(array)
        console.log(`duplicate number from array are : ${data}`); // to check the result
    
        //output
        //duplicate number from array are : 3,5,7,9,11,13,14,14,14

//----------------------------------------------------------------------------------------------------------------------------

    //##### 2nd Approach 
    // using set() and has() method

    const uniqueSet = new Set(array);
    // console.log('uniqueSet:', uniqueSet)
    // uniqueSet: Set(14) { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14 }
    const filteredElements = array.filter(currentValue => {
          if (uniqueSet.has(currentValue))  
             uniqueSet.delete(currentValue);
          else 
             return currentValue;
       }
    );
    console.log(`method 2 duplicate number from array are : ${filteredElements}`); // to check the result

    // output
    // method 2 : duplicate number from array are : 3,5,7,9,11,13,14,14,14
//####################################################################################################################################
  // ##To count the occurrence of duplicate numbers in a JavaScript array and get the count for each number
  //####################################################################################################################################
  function countDuplicateNumbers(arr) {
    const countMap = {}; // Object to store counts
    
    // Iterate through the array
    for (const num of arr) {
      if (countMap[num] === undefined) {
        // If the number is not in the countMap, initialize the count to 1
        countMap[num] = 1;
      } else {
        // If the number is already in the countMap, increment the count
        countMap[num]++;
      }
    }
    
    // Create an array to store the results
    const result = [];
    
    // Iterate through the countMap to format the results
    for (const num in countMap) {
      if (countMap.hasOwnProperty(num)) {
        result.push({
          number: parseInt(num),
          count: countMap[num]
        });
      }
    }
    
    return result;
  }
  
  const arr_3 = [1, 2, 2, 3, 4, 4, 4, 5, 6, 6];
  const duplicateCounts = countDuplicateNumbers(arr_3);
  console.log(duplicateCounts);

  // output
  [
    { number: 1, count: 1 },
    { number: 2, count: 2 },
    { number: 3, count: 1 },
    { number: 4, count: 3 },
    { number: 5, count: 1 },
    { number: 6, count: 2 }
  ]
  
  // ------------------------------OR---------------------------------------------------------
  function countDuplicateOccurrences(arr) {
    const countMap = {}; // Create an object to store counts

    // Iterate through the array
    for (const num of arr) {
        // If the number is in the countMap, increment its count
        if (countMap[num] !== undefined) {
            countMap[num]++;
        } else {
            // If the number is not in the countMap, initialize its count to 1
            countMap[num] = 1;
        }
    }

    // Create an object to store the results
    const result = {};

    // Iterate through the countMap to filter out duplicates
    for (const num in countMap) {
        if (countMap[num] > 1) {
            result[num] = countMap[num]; // Store the count in the result object
        }
    }

    return result;
}

const arr_5 = [1, 2, 2, 3, 4, 4, 4, 5, 6, 6];
const duplicateCounts_12 = countDuplicateOccurrences(arr_5);
console.log(duplicateCounts_12); // Output: { '2': 2, '4': 3, '6': 2 }

  
  //####################################################################################################################################
  //3. ##JavaScript, How do you remove duplicate number on a given integer array?
  //####################################################################################################################################

    let arr = ["scale", "happy", "strength", "peace", "happy", "happy" ,"scale", "happy", "strength", "peace", "happy", "happy" ];

    function removeDuplicates(arr) {
      return arr.filter((item,index) => arr.indexOf(item) === index);
    }
    console.log('remove duplicated using filter',removeDuplicates(arr));

//----------------------------------------------------------------------------------------------------------------------------
    function removeDuplicates2(arr) {
        let unique = [];
        arr.filter(element => {
            if (!unique.includes(element)) {
                unique.push(element);
            }
        });
        return unique;
    }

  console.log(removeDuplicates2(arr));
//----------------------------------------------------------------------------------------------------------------------------


  function removeDuplicates3(arr) {
    let unique = arr.reduce(function (acc, curr) {
        if (!acc.includes(curr))
            acc.push(curr);
        return acc;
    }, []);
    return unique;
  }

  console.log(removeDuplicates3(arr));

//----------------------------------------------------------------------------------------------------------------------------


  function removeDuplicates4(arr) {
    let unique = [];
    for(i=0; i < arr.length; i++){ 
        if(unique.indexOf(arr[i]) === -1) { 
            unique.push(arr[i]); 
        } 
    }
    return unique;
  }

  console.log(removeDuplicates4(arr));

}catch(error){
   console.log('error:', error)
}


  //##################################################################################################################################################################################
  //4. ##JavaScript, Given an integer array, find the contiguous subarray (containing at least one number) which has the largest sum and return its sum? (solution)
  //##################################################################################################################################################################################
  //method1
  const maxSubArray = (nums) => {
    // initiate two variable, maxSum for total max, sum for current max
    let maxSum = -Infinity  // maxSum: -Infinity

    console.log('maxSum:', maxSum)
    let currentSum = 0
    for(let i = 0; i < nums.length; i++){ 
        currentSum = Math.max(nums[i], currentSum + nums[i])
        maxSum = Math.max(currentSum, maxSum)
    }
    console.log('maxSum:', maxSum)
    return maxSum
  }
  let arrdata = [1,-2,3,-4,5,-6,7,9,9,-8,9,-10,-11];
  maxSubArray(arrdata)

  // Example:
  // Input: [1,-2,3,-4,5,-6,7,9,9,-8,9,-10,-11];
  // Explanation: [3,-4,5,-6,7,9,9,-8,9,] has the largest sum = 26.
  // Output  maxSum: 26
//----------------------------------------------------------------------------------------------------------------------
//***-------------easy way no confusion --------------------******//
//************************************************ */
  const maxSubArray4 = (nums) => {
    // initiate two variable, maxSum for total max, sum for current max
    let maxSum = 0; 
    let currentSum = 0
    nums.filter(ele=>{
          currentSum = Math.max(ele, currentSum + ele)
          maxSum = Math.max(currentSum, maxSum)
    })
    console.log('maxSum:', maxSum)
    return maxSum
  }
  let arrdata4 = [1,-2,-3,4,5,1];
  maxSubArray4(arrdata4)

  //maxSum: 10

  //----------------------------------------------------------------------------------------------------------------------------
  //method2

  //******** Brute Force Approach******* //
  // We can calculate the sum of every possible subarray and the maximum of those would be the solution. 
  // Very obvious, but not so good solution, since it has time complexity O(n²) and space complexity: O(1).


  const maxSubArray2 = (nums) => {
    let maxSum = Number.MIN_SAFE_INTEGER;    // maxSum22: -9007199254740991

    for (let i = 0; i < nums.length; i++) {
      let currentSubArraySum = 0;

      for (let j = i; j < nums.length; j++) {
        currentSubArraySum += nums[j];
        maxSum = Math.max(maxSum, currentSubArraySum);
      }
    }
    console.log('maxSum 2:', maxSum);
    return maxSum;
  };
  maxSubArray2(arrdata)


  // Example:
  // Input: [1,-2,3,-4,5,-6,7,9,9,-8,9,-10,-11];
  // Explanation: [3,-4,5,-6,7,9,9,-8,9,] has the largest sum = 26.
  // Output  maxSum: 26
  // maxSum 2: 26
  //----------------------------------------------------------------------------------------------------------------------------

  //method3

  // ***********Kadane’s Algorithm*******//
  // We would start from the first element of array nums, 
  // calculate the sum of every possible subarray ending with the element nums[i + n]. 
  // More difficult to understand solution but has better time complexity O(n) and same space complexity: O(1).

  const maxSubArray3 = (nums) => {
    let maxSum = Number.MIN_SAFE_INTEGER;
    let runningSum = 0;

    for (let i = 0; i < nums.length; i++) {
      runningSum = Math.max(runningSum + nums[i], nums[i]);
      maxSum = Math.max(maxSum, runningSum);
    }
    console.log('maxSum:3', maxSum)
    return maxSum;
  };
  maxSubArray3(arrdata)

  // Example:
  // Input: [1,-2,3,-4,5,-6,7,9,9,-8,9,-10,-11];
  // Explanation: [3,-4,5,-6,7,9,9,-8,9,] has the largest sum = 26.
  // Output  maxSum: 26
  // maxSum 2: 26

  // ===================================================================================================================
  // Given an integer array, find the contiguous subarray with the largest sum and return its sum? (Solution)
  // ===================================================================================================================
  function maxSubarraySum(nums) {
    let maxSum = nums[0];
    let currentSum = nums[0];

    for (let i = 1; i < nums.length; i++) {
      currentSum = Math.max(nums[i], currentSum + nums[i]);
      maxSum = Math.max(maxSum, currentSum);
    }

    return maxSum;
  }

  // Test
  const integerArray2 = [-2, 1, -3, 4, -1, 2, 1, -5, 4];
  console.log(maxSubarraySum(integerArray2)); // Output: 6 (Subarray: [4, -1, 2, 1])



  //##################################################################################################################################################################################
  //5. ##How do you find the largest and smallest number in an unsorted integer array? (solution)
  //##################################################################################################################################################################################
  //method1

  const numberArr = [23, 122, 1, 23, 4, 56];
  const highest = Math.max(...numberArr);
  const lowest = Math.min(...numberArr);
  console.log("Highest Number: " + highest); // Highest Number: 122
  console.log("Lowest Number: " + lowest); // Lowest Number: 1

  // Output  
  //   Highest Number: 122
  // Lowest Number: 1

  //----------------------------------------------------------------------------------------------------------------------------
  //method2

  const arr1 = [112, 24, 31, 44, 101, 203, 33, 56];
  const findMaxMin = (arr) => {
    let max = arr[0];
    let min = arr[0];
    for(let i = 0; i < arr.length; i++) {
        if(arr[i] > max) {
          max = arr[i];
        }
        else if (arr[i] < min) {
          min = arr[i];
        }
    };
    return {
        min, max
    };
  };
  console.log(findMaxMin(arr1));

  // Output  
  // { min: 24, max: 203 }


  //----------------------------------------------------------------------------------------------------------------------------
  //method3

  var numbers = [2, 4, 9, 2, 0, 16, 24];
  var sorted = numbers.slice().sort(function(a, b) {
    return a - b;
  });

  var smallest = sorted[0],                      
      secondSmallest = sorted[1],                
      secondLargest = sorted[sorted.length - 2], 
      largest  = sorted[sorted.length - 1];

  console.log('Smallest: ' + smallest);
  console.log('Second Smallest: ' + secondSmallest);
  console.log('Second Largest: ' + secondLargest);
  console.log('Largest: ' + largest);


  //##################################################################################################################################################################################
  // find and remove 2nd largest element In 4 lines
  //##################################################################################################################################################################################
  let array = [9,534,5,7,5,3,5,67,8,9,55,3,24,3];
  let sorted = array.sort((a,b)=> a-b )
  secondLastelementIndex = sorted.length-2 ;
  array.splice(secondLastelementIndex,1)
  console.log("sorted array ",sorted)
  console.log("secondLastelementIndex",secondLastelementIndex)
  console.log("Final array ",array)

  // output
  // --------------
  // sorted array  [3,   3, 3, 5,  5,  5,7,   8, 9, 9, 24, 55,67, 534]
  // secondLastelementIndex 12
  // Final array  [3, 3, 3, 5,  5,  5, 7, 8, 9, 9, 24, 55, 534 ]


  //##################################################################################################################################################################################
  // Find if there is a subarray with 0 sum.
  //  Input: [4, 2, -3, 1, 6]
  //  Output: true
  //##################################################################################################################################################################################

  //************Easy method*************//
    function hasSubarrayWithZeroSum2(arr) {
    let seen = new Set();
    let sum = 0; 
    let found = false;
     arr.filter((ele)=>{
        sum =  sum + ele ;
        if(sum == 0|| seen.has(sum) ){
            found =true;
        }
        seen.add(sum)
    })
     return found;
  }
 let array22 = [4, 2, -3, 3, 6];
 console.log(hasSubarrayWithZeroSum2(array22)); // Output: true (Subarray: [2, -3, 1])
 
// ----------------------------------------------------------------------------------
  function hasSubarrayWithZeroSum(arr) {
    const seen = new Set();
    let sum = 0;
  
    for (let i = 0; i < arr.length; i++) {
      sum += arr[i];
  
      if (sum === 0 || seen.has(sum)) {
        return true;
      }
  
      seen.add(sum);
    }
  
    return false;
  }
  
  // Test the function
  const array1 = [4, 2, -3, 1, 6];
  console.log(hasSubarrayWithZeroSum(array1)); // Output: true (Subarray: [2, -3, 1])
  
  const array2 = [4, 2, 0, 1, 6];
  console.log(hasSubarrayWithZeroSum(array2)); // Output: true (Subarray: [0])
  
  const array3 = [4, 2, -3, 1, 6];
  console.log(hasSubarrayWithZeroSum(array3)); // Output: true (Subarray: [-3, 1, 6])
  
  const array4 = [1, 2, 3];
  console.log(hasSubarrayWithZeroSum(array4)); // Output: false





  
// ===================================================================================================================
// How do you search a target value in a rotated array? (Solution)
// ===================================================================================================================
function searchInRotatedArray(nums, target) {
  let left = 0;
  let right = nums.length - 1;

  while (left <= right) {
    const mid = Math.floor((left + right) / 2);
    if (nums[mid] === target) {
      return mid;
    }

    if (nums[left] <= nums[mid]) {
      if (target >= nums[left] && target < nums[mid]) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    } else {
      if (target > nums[mid] && target <= nums[right]) {
        left = mid + 1;
      } else {
        right = mid - 1;
      }
    }
  }

  return -1;
}

// Test
const rotatedArray = [4, 5, 6, 7, 0, 1, 2];
const target = 0;
console.log(searchInRotatedArray(rotatedArray, target)); // Output: 4


  
// ===================================================================================================================
// Given an unsorted array of integers, find the length of the longest consecutive elements sequence? (Solution)
// ===================================================================================================================
function longestConsecutive(nums) {
  const numSet = new Set(nums);
  let longestStreak = 0;

  for (const num of numSet) {
    if (!numSet.has(num - 1)) {
      let currentNum = num;
      let currentStreak = 1;

      while (numSet.has(currentNum + 1)) {
        currentNum += 1;
        currentStreak += 1;
      }

      longestStreak = Math.max(longestStreak, currentStreak);
    }
  }

  return longestStreak;
}

// Test
const unsortedArray = [100, 4, 200, 1, 3, 2];
console.log(longestConsecutive(unsortedArray)); // Output: 4
  
// ===================================================================================================================
// How is an integer array sorted in place using the quicksort algorithm? (Solution)
// ===================================================================================================================
function quickSort(arr, left = 0, right = arr.length - 1) {
  if (left < right) {
    const pivotIndex = partition(arr, left, right);
    quickSort(arr, left, pivotIndex - 1);
    quickSort(arr, pivotIndex + 1, right);
  }
}

function partition(arr, left, right) {
  const pivot = arr[right];
  let partitionIndex = left;

  for (let i = left; i < right; i++) {
    if (arr[i] < pivot) {
      swap(arr, i, partitionIndex);
      partitionIndex++;
    }
  }

  swap(arr, partitionIndex, right);
  return partitionIndex;
}

function swap(arr, i, j) {
  const temp = arr[i];
  arr[i] = arr[j];
  arr[j] = temp;
}

// Test
var integerArray = [8, 3, 1, 7, 0, 10, 2];
quickSort(integerArray);
console.log(integerArray); // Output: [0, 1, 2, 3, 7, 8, 10]

// ===================================================================================================================
// How do you remove duplicates from an array in place? (Solution)
// ===================================================================================================================
function removeDuplicates(nums) {
  if (nums.length === 0) return 0;

  let currentIndex = 1;

  for (let i = 1; i < nums.length; i++) {
    if (nums[i] !== nums[i - 1]) {
      nums[currentIndex] = nums[i];
      currentIndex++;
    }
  }

  return currentIndex;
}

// Test
const duplicateArray = [1, 1, 2, 2, 3, 4, 4, 5];
const newLength = removeDuplicates(duplicateArray);
console.log(newLength); // Output: 5 (Array: [1, 2, 3, 4, 5, ...])

// ===================================================================================================================
// How do you reverse an array in place in JavaScript? (Solution)
// ===================================================================================================================
//------------------Easy method------------------------------------------//
function reverseArrayInPlace(arr) {
  let left = 0;
  let right = arr.length - 1;

  while (left < right) {
    const temp = arr[left];
    arr[left] = arr[right];
    arr[right] = temp;

    left++;
    right--;
  }
}

// Test
const arrayToReverse = [1, 2, 3, 4, 5];
reverseArrayInPlace(arrayToReverse);
console.log(arrayToReverse); // Output: [5, 4, 3, 2, 1]


// ===================================================================================================================
// How are duplicates removed from an array without using any library? (Solution)
// ===================================================================================================================
//-----------Easy method---------------------------
function removeDuplicatesFromArray(arr) {
  const uniqueArray = [];

  for (const num of arr) {
    if (!uniqueArray.includes(num)) {
      uniqueArray.push(num);
    }
  }

  return uniqueArray;
}

// Test
const arrayWithDuplicates = [1, 2, 2, 3, 4, 4, 5];
const newArray = removeDuplicatesFromArray(arrayWithDuplicates);
console.log(newArray); // Output: [1, 2, 3, 4, 5]



// ===================================================================================================================
// How to convert a byte array to String? (Solution)
// ===================================================================================================================
const byteArray = [72, 101, 108, 108, 111]; // ASCII values for 'Hello'
const str = String.fromCharCode(...byteArray);
console.log(str); // Output: "Hello"
// ===================================================================================================================
// How do you perform a binary search in a given array? (Solution, continued)
// ===================================================================================================================
//-----------Easy method---------------------------
function binarySearch(nums, target) {

  let left = 0;
  let right = nums.length - 1;

  while (left <= right) {
    const mid = Math.floor((left + right) / 2);

    if (nums[mid] === target) {
      return mid; // Target found
    } else if (nums[mid] < target) {
      left = mid + 1; // Search the right half
    } else {
      right = mid - 1; // Search the left half
    }
  }

  return -1; // Target not found
}

// Test
var sortedArray = [1, 3, 5, 7, 9, 11, 13];
var targetValue = 7;
console.log(binarySearch(sortedArray, targetValue)); // Output: 3 (Index of target 7)



// ===================================================================================================================
// How to find the median of two sorted arrays? (Solution)
// ===================================================================================================================
function findMedianSortedArrays(nums1, nums2) {
  const merged = mergeSortedArrays(nums1, nums2);
  const mid = Math.floor(merged.length / 2);
  
  if (merged.length % 2 === 0) {
    return (merged[mid - 1] + merged[mid]) / 2;
  } else {
    return merged[mid];
  }
}

function mergeSortedArrays(arr1, arr2) {
  const merged = [];
  let i = 0;
  let j = 0;

  while (i < arr1.length && j < arr2.length) {
    if (arr1[i] < arr2[j]) {
      merged.push(arr1[i]);
      i++;
    } else {
      merged.push(arr2[j]);
      j++;
    }
  }

  return merged.concat(arr1.slice(i), arr2.slice(j));
}

// Test
const nums1 = [1, 3];
const nums2 = [2];
console.log(findMedianSortedArrays(nums1, nums2)); // Output: 2.0

// ===================================================================================================================
// How to rotate an array left and right by a given number K? (Solution)
// ===================================================================================================================

function rotateArrayLeft(arr, k) {
  k %= arr.length; // Handle cases where k > arr.length
  reverseArray(arr, 0, k - 1);
  reverseArray(arr, k, arr.length - 1);
  reverseArray(arr, 0, arr.length - 1);
}

function rotateArrayRight(arr, k) {
  k %= arr.length; // Handle cases where k > arr.length
  reverseArray(arr, 0, arr.length - 1);
  reverseArray(arr, 0, k - 1);
  reverseArray(arr, k, arr.length - 1);
}

function reverseArray(arr, left, right) {
  while (left < right) {
    const temp = arr[left];
    arr[left] = arr[right];
    arr[right] = temp;
    left++;
    right--;
  }
}

// Test
const arrayToRotate = [1, 2, 3, 4, 5, 6, 7];
rotateArrayLeft(arrayToRotate, 3);
console.log(arrayToRotate); // Output: [4, 5, 6, 7, 1, 2, 3]

rotateArrayRight(arrayToRotate, 2);
console.log(arrayToRotate); // Output: [2, 3, 4, 5, 6, 7, 1]





// ===================================================================================================================
// How do you find duplicates from an unsorted array? (Solution)
// ===================================================================================================================
//------------------Nice technique to find duplicates---------------------------
function findDuplicates(arr) {
  const duplicates = [];
  const numCount = {};

  for (const num of arr) {
    if (numCount[num]) {
      duplicates.push(num);
    } else {
      numCount[num] = true;
    }
  }

  return duplicates;
}

// Test
const unsortedWithDuplicates = [3, 1, 2, 2, 1, 4, 5, 4];
const duplicateNumbers = findDuplicates(unsortedWithDuplicates);
console.log(duplicateNumbers); // Output: [1, 2, 4]


// ===================================================================================================================
  // Given an array of integers sorted in ascending order, find the starting and ending position of a given value? (Solution)
// ===================================================================================================================
//------------------using inbuilt index of  methods---------------------------
function searchRange12(nums, target) {
  const startIndex = nums.indexOf(target);
  const endIndex = nums.lastIndexOf(target);
  
  return [startIndex, endIndex];
}

const sortedArray12 = [2, 3, 3, 5, 7, 7, 8, 8, 8, 10];
const targetValue12 = 7;

const result3 = searchRange12(sortedArray12, targetValue12);
console.log(result3); // Output: [4, 5]

// --------------using for loop only/ tough method---------------------------------------------
  function searchRange(nums, target) {
    const leftIndex = findFirstIndex(nums, target);
    if (leftIndex === -1) {
      return [-1, -1];
    }
    
    const rightIndex = findLastIndex(nums, target);
    return [leftIndex, rightIndex];
  }
  
  function findFirstIndex(nums, target) {
    let left = 0;
    let right = nums.length - 1;
    let result = -1;
  
    while (left <= right) {
      const mid = Math.floor((left + right) / 2);
      if (nums[mid] >= target) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
      if (nums[mid] === target) {
        result = mid;
      }
    }
  
    return result;
  }
  
  function findLastIndex(nums, target) {
    let left = 0;
    let right = nums.length - 1;
    let result = -1;
  
    while (left <= right) {
      const mid = Math.floor((left + right) / 2);
      if (nums[mid] <= target) {
        left = mid + 1;
      } else {
        right = mid - 1;
      }
      if (nums[mid] === target) {
        result = mid;
      }
    }
  
    return result;
  }
  
  // Test
  const sortedArray = [5, 7, 7, 8, 8, 10];
  const targetValue = 8;
  console.log(searchRange(sortedArray, targetValue)); // Output: [3, 4]

// ===================================================================================================================
   // drwa this pattern 
  // 1
  // 2 3
  // 4 5 6
  // 7 8 9 10
  // 11 12 13 14 15
// ===================================================================================================================


function generatePattern(rows) {
  let num = 1;
  
  for (let i = 1; i <= rows; i++) {
      let row = '';
      for (let j = 1; j <= i; j++) {
          row += num + ' ';
          num++;
      }
      console.log(row);
  }
}

generatePattern(5); // Change the argument to the number of rows you want



// ===================================================================================================================
// *
// * *
// * * *
// * * * *
// * * * * *
// ===================================================================================================================




function drawPattern(rows) {
    for (let i = 1; i <= rows; i++) {
        let pattern = '';
        for (let j = 1; j <= i; j++) {
            pattern += '* ';
        }
        console.log(pattern);
    }
}

const numRows = 5; // You can change this value for different number of rows
drawPattern(numRows);


// ===================================================================================================================
  //       *
  //     *   *
  //   *   *   *
  // *   *   *   *
// ===================================================================================================================
  
  function drawPattern(rows) {
    for (let i = 1; i <= rows; i++) {
        let pattern = '';
        
        // Add spaces before each asterisk
        for (let j = 1; j <= rows - i; j++) {
            pattern += '  ';
        }
        
        // Add asterisks
        for (let j = 1; j <= i; j++) {
            pattern += '*   ';
        }
        
        console.log(pattern);
    }
}

let numRows2 = 4; // You can change this value for different number of rows
drawPattern(numRows);








// ===================================================================================================================
//string related question 
// ===================================================================================================================





// How do you reverse a given string in place? (solution)

function reverseString(str) {
  return str.split('').reverse().join('');
}



// How do you print duplicate characters from a string? (solution)

function findDuplicates(str) {
  let charMap = {};
  let duplicates = [];

  for (let char of str) {
      charMap[char] = (charMap[char] || 0) + 1;
      if (charMap[char] === 2) {
          duplicates.push(char);
      }
  }

  return duplicates;
}

// How do you check if two strings are anagrams of each other? (solution)
function areAnagrams(str1, str2) {
  return str1.split('').sort().join('') === str2.split('').sort().join('');
}

// How do you find all the permutations of a string? (solution)
function generatePermutations(str) {
  if (str.length <= 1) {
      return [str];
  }

  let permutations = [];

  for (let i = 0; i < str.length; i++) {
      let char = str[i];
      let remainingChars = str.slice(0, i) + str.slice(i + 1);
      let subPermutations = generatePermutations(remainingChars);
      
      for (let subPerm of subPermutations) {
          permutations.push(char + subPerm);
      }
  }

  return permutations;
}

// How can a given string be reversed using recursion? (solution)
function reverseStringRecursion(str) {
  if (str === '') {
      return '';
  } else {
      return reverseStringRecursion(str.substr(1)) + str[0];
  }
}

// How do you check if a string contains only digits? (solution)
function containsOnlyDigits(str) {
  return /^\d+$/.test(str);
}

// How do you find duplicate characters in a given string? (solution)
function findDuplicateCharacters(str) {
  let charMap = {};
  let duplicates = [];

  for (let char of str) {
      if (charMap[char]) {
          charMap[char]++;
      } else {
          charMap[char] = 1;
      }
  }

  for (let char in charMap) {
      if (charMap[char] > 1) {
          duplicates.push(char);
      }
  }

  return duplicates;
}


// How do you count a number of vowels and consonants in a given string? (solution)
function countVowelsConsonants(str) {
  const vowels = 'aeiouAEIOU';
  let vowelCount = 0;
  let consonantCount = 0;

  for (let char of str) {
      if (vowels.includes(char)) {
          vowelCount++;
      } else if (char.match(/[a-zA-Z]/)) {
          consonantCount++;
      }
  }

  return { vowels: vowelCount, consonants: consonantCount };
}

// How do you count the occurrence of a given character in a string? (solution)
function countCharacterOccurrence(str, char) {
  let count = 0;

  for (let c of str) {
      if (c === char) {
          count++;
      }
  }

  return count;
}

// How do you print the first non-repeated character from a string? (solution)
function findFirstNonRepeated(str) {
  let charMap = {};

  for (let char of str) {
      charMap[char] = (charMap[char] || 0) + 1;
  }

  for (let char of str) {
      if (charMap[char] === 1) {
          return char;
      }
  }

  return null;
}


// How do you convert a given String into int like the atoi()? (solution)

function atoi(str) {
  return parseInt(str) || 0;
}




//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Find Maximum Subarray Sum: Given an array of integers, find the contiguous subarray with the largest sum.
// Find Maximum Subarray Sum
function maxSubarraySum(nums) {
  if (nums.length === 0) {
    return 0;
  }

  let maxSum = nums[0];
  let currentSum = nums[0];

  for (let i = 1; i < nums.length; i++) {
    currentSum = Math.max(nums[i], currentSum + nums[i]);
    maxSum = Math.max(maxSum, currentSum);
  }

  return maxSum;
}

const nums5 = [-2, 1, -3, 4, -1, 2, 1, -5, 4];
const result6 = maxSubarraySum(nums5);
console.log(`Maximum Subarray Sum: ${result6}`);
//=====================================================================================================================================================
// target sum from an given array using js
//=====================================================================================================================================================
// Method 1: Using Nested Loops (Brute Force)
function findTargetSum(arr, target) {
  for (let i = 0; i < arr.length - 1; i++) {
      for (let j = i + 1; j < arr.length; j++) {
          if (arr[i] + arr[j] === target) {
              return [arr[i], arr[j]];
          }
      }
  }
  return null; // If no such pair is found
}

const arr3 = [2, 7, 11, 15];
const target3 = 9;
const result4 = findTargetSum(arr3, target3);
console.log(result4); // Output: [2, 7]
// ------------------------------------------------------------------------------
// ------------------------------------------------------------------------------

//  OR  method 2 
// Method 2: Using a Hash Table (Optimized)
function findTargetSum(arr, target) {
  const numMap = {}; // Create an empty object to store seen numbers
  for (let i = 0; i < arr.length; i++) {
      const complement = target - arr[i];
      if (numMap[complement] !== undefined) {
          return [complement, arr[i]];
      }
      numMap[arr[i]] = i;
  }
  return null; // If no such pair is found
}

const arr4 = [2, 7, 11, 15];
const targe4 = 9;
const result5 = findTargetSum(arr4, targe4);
console.log(result5); // Output: [2, 7]

//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Subarray with Given Sum: Given an array of positive integers and a target sum, find if there exists a subarray with the given sum.
function hasSubarrayWithSum(arr, targetSum) {
  let currentSum = 0;
  let start = 0;

  for (let i = 0; i < arr.length; i++) {
      currentSum += arr[i];

      while (currentSum > targetSum && start <= i) {
          currentSum -= arr[start];
          start++;
      }

      if (currentSum === targetSum) {
          return true;
      }
  }

  return false;
}

const arr7 = [1, 4, 20, 3, 10, 5];
const targetSum7 = 33;

if (hasSubarrayWithSum(arr7, targetSum7)) {
  console.log("Subarray with the given sum exists.");
} else {
  console.log("Subarray with the given sum does not exist.");
}
//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Longest Increasing Subarray: Find the longest increasing subarray within an array of integers.

function longestIncreasingSubarray(arr) {
  let maxLength = 1;
  let currentLength = 1;

  for (let i = 1; i < arr.length; i++) {
      if (arr[i] > arr[i - 1]) {
          currentLength++;
          maxLength = Math.max(maxLength, currentLength);
      } else {
          currentLength = 1;
      }
  }

  return maxLength;
}

const arr_1 = [1, 2, 3, 2, 4, 6, 8, 1, 2, 3];
const longestLength = longestIncreasingSubarray(arr_1);

console.log("Longest increasing subarray length:", longestLength);
//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Count Subarrays with Given Sum: Count the number of subarrays within an array of integers that sum up to a given value.

function countSubarraysWithSum(arr, targetSum) {
  let count = 0;
  let currentSum = 0;
  const sumMap = new Map();

  for (let i = 0; i < arr.length; i++) {
      currentSum += arr[i];

      if (currentSum === targetSum) {
          count++;
      }

      if (sumMap.has(currentSum - targetSum)) {
          count += sumMap.get(currentSum - targetSum);
      }

      if (!sumMap.has(currentSum)) {
          sumMap.set(currentSum, 0);
      }

      sumMap.set(currentSum, sumMap.get(currentSum) + 1);
  }

  return count;
}

const arr_2 = [10, 2, -2, -20, 10];
const targetSum_1 = -10;

const result_1 = countSubarraysWithSum(arr_2, targetSum_1);
console.log(`Number of subarrays with sum ${targetSum_1}: ${result_1}`);


//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Smallest Subarray with Sum Greater than X: Find the length of the smallest subarray with a sum greater than a given value.

function smallestSubarrayWithSumGreater(arr, targetSum) {
  let minLength = Infinity;
  let windowSum = 0;
  let windowStart = 0;

  for (let windowEnd = 0; windowEnd < arr.length; windowEnd++) {
      windowSum += arr[windowEnd];

      while (windowSum > targetSum) {
          minLength = Math.min(minLength, windowEnd - windowStart + 1);
          windowSum -= arr[windowStart];
          windowStart++;
      }
  }

  return minLength === Infinity ? 0 : minLength;
}

const arr = [1, 4, 45, 6, 0, 19];
const targetSum = 51;

const result = smallestSubarrayWithSumGreater(arr, targetSum);
console.log(`The length of the smallest subarray with sum greater than ${targetSum} is ${result}`);

//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Subarray Product Less Than K: Given an array of positive integers and an integer K, count the number of contiguous subarrays where the product of all the elements is less than K.

function countSubarraysWithProductLessThanK(arr, k) {
  if (k <= 1) {
      return 0;
  }

  let count = 0;
  let product = 1;
  let left = 0;

  for (let right = 0; right < arr.length; right++) {
      product *= arr[right];

      while (product >= k) {
          product /= arr[left];
          left++;
      }

      count += (right - left + 1);
  }

  return count;
}

const arr = [10, 5, 2, 6];
const k = 100;

const result = countSubarraysWithProductLessThanK(arr, k);
console.log("Number of contiguous subarrays with product less than K:", result);


//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Subarray Division: Given an array of integers and a target sum, find the number of ways to divide the array into subarrays such that each subarray has a sum equal to the target sum.
function countSubarrayDivisions(arr, targetSum) {
  const prefixSum = [0];
  for (let i = 0; i < arr.length; i++) {
      prefixSum[i + 1] = prefixSum[i] + arr[i];
  }

  const sumMap = new Map();
  let count = 0;

  for (let i = 0; i < prefixSum.length; i++) {
      const complement = prefixSum[i] - targetSum;

      if (sumMap.has(complement)) {
          count += sumMap.get(complement);
      }

      if (prefixSum[i] === targetSum) {
          count++;
      }

      if (!sumMap.has(prefixSum[i])) {
          sumMap.set(prefixSum[i], 1);
      } else {
          sumMap.set(prefixSum[i], sumMap.get(prefixSum[i]) + 1);
      }
  }

  return count;
}

const arr = [1, 1, 2, 1, 1];
const targetSum = 2;

const ways = countSubarrayDivisions(arr, targetSum);
console.log("Number of ways to divide the array:", ways);

//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Longest Subarray with Equal 0s and 1s: Find the length of the longest subarray within an array of 0s and 1s such that the number of 0s and 1s are equal.
function maxLengthSubarrayWithEqual01(nums) {
  const countMap = new Map();
  countMap.set(0, -1); // To handle edge case where subarray starts from index 0
  let maxLen = 0;
  let count = 0;

  for (let i = 0; i < nums.length; i++) {
      if (nums[i] === 0) {
          count--;
      } else {
          count++;
      }

      if (countMap.has(count)) {
          maxLen = Math.max(maxLen, i - countMap.get(count));
      } else {
          countMap.set(count, i);
      }
  }

  return maxLen;
}

const nums = [0, 1, 0, 1, 1, 0, 0];
const maxLength = maxLengthSubarrayWithEqual01(nums);
console.log("Length of the longest subarray with equal 0s and 1s:", maxLength);

//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Maximum Length of Subarray with Equal 0s and 1s: Find the maximum length of a subarray within an array of 0s and 1s where the number of 0s and 1s are equal.
function maxLengthSubarrayWithEqual01(arr) {
  const sumToIndex = new Map();
  let maxLength = 0;
  let sum = 0;

  sumToIndex.set(0, -1);

  for (let i = 0; i < arr.length; i++) {
      sum += arr[i] === 0 ? -1 : 1;

      if (sumToIndex.has(sum)) {
          maxLength = Math.max(maxLength, i - sumToIndex.get(sum));
      } else {
          sumToIndex.set(sum, i);
      }
  }

  return maxLength;
}

const arr = [0, 1, 1, 0, 1, 0, 0];
const maxLength = maxLengthSubarrayWithEqual01(arr);

console.log("Maximum length of subarray with equal 0s and 1s:", maxLength);
//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Subarray Sum Equals K: Given an array of integers and an integer K, find the total number of continuous subarrays whose sum equals K.
function subarraySum(nums, k) {
  const sumMap = new Map();
  sumMap.set(0, 1); // Initialize with sum 0 to handle subarrays starting from index 0
  let count = 0;
  let sum = 0;

  for (let i = 0; i < nums.length; i++) {
      sum += nums[i];

      if (sumMap.has(sum - k)) {
          count += sumMap.get(sum - k);
      }

      if (!sumMap.has(sum)) {
          sumMap.set(sum, 0);
      }
      sumMap.set(sum, sumMap.get(sum) + 1);
  }

  return count;
}

const nums = [1, 1, 1];
const k = 2;
const result = subarraySum(nums, k);
console.log(`Number of continuous subarrays with sum ${k}: ${result}`);

//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Subarray Sum Divisible by K: Given an array of integers and an integer K, find the total number of continuous subarrays whose sum is divisible by K.
function countSubarraysDivisibleByK(arr, K) {
  const prefixSumMod = new Map();
  prefixSumMod.set(0, 1); // Initialize with 0 to handle subarrays starting at index 0
  let prefixSum = 0;
  let count = 0;

  for (let i = 0; i < arr.length; i++) {
      prefixSum = (prefixSum + arr[i]) % K;

      if (prefixSum < 0) {
          prefixSum += K; // Ensure the remainder is positive
      }

      if (prefixSumMod.has(prefixSum)) {
          count += prefixSumMod.get(prefixSum);
      }

      prefixSumMod.set(prefixSum, (prefixSumMod.get(prefixSum) || 0) + 1);
  }

  return count;
}

const arr = [4, 5, 0, -2, -3, 1];
const K = 5;

const result = countSubarraysDivisibleByK(arr, K);
console.log(`Total number of subarrays whose sum is divisible by ${K}: ${result}`);
//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Maximum Length of Subarray with Sum Zero: Find the maximum length of a subarray within an array of integers whose sum is zero.
function maxSubarrayLengthWithSumZero(arr) {
  const sumMap = new Map();
  let maxLen = 0;
  let currentSum = 0;

  for (let i = 0; i < arr.length; i++) {
      currentSum += arr[i];

      if (currentSum === 0) {
          maxLen = i + 1;
      }

      if (sumMap.has(currentSum)) {
          maxLen = Math.max(maxLen, i - sumMap.get(currentSum));
      } else {
          sumMap.set(currentSum, i);
      }
  }

  return maxLen;
}

const arr = [15, -2, 2, -8, 1, 7, 10, 23];
const maxLength = maxSubarrayLengthWithSumZero(arr);

console.log("Maximum length of subarray with sum zero:", maxLength);
//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Maximum Average Subarray: Given an array of integers, find the contiguous subarray with the maximum average.
function findMaxAverageSubarray(nums, k) {
  let maxSum = 0;
  let currentSum = 0;

  for (let i = 0; i < k; i++) {
      currentSum += nums[i];
  }

  maxSum = currentSum;

  for (let i = k; i < nums.length; i++) {
      currentSum = currentSum - nums[i - k] + nums[i];
      maxSum = Math.max(maxSum, currentSum);
  }

  return maxSum / k;
}

const nums = [1, 12, -5, -6, 50, 3];
const k = 4;

const maxAverage = findMaxAverageSubarray(nums, k);
console.log("Maximum average subarray:", maxAverage);
//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Subarray Sort to Make Array Sorted: Find the shortest subarray that, when sorted, makes the whole array sorted.
function findUnsortedSubarray(arr) {
  const n = arr.length;
  let left = 0, right = n - 1;
  
  // Find the left bound of the unsorted subarray
  while (left < n - 1 && arr[left] <= arr[left + 1]) {
      left++;
  }
  
  if (left === n - 1) {
      // The array is already sorted
      return 0;
  }
  
  // Find the right bound of the unsorted subarray
  while (right > 0 && arr[right] >= arr[right - 1]) {
      right--;
  }
  
  // Find the minimum and maximum elements within the unsorted subarray
  let minWithinSubarray = Number.MAX_SAFE_INTEGER;
  let maxWithinSubarray = Number.MIN_SAFE_INTEGER;
  for (let i = left; i <= right; i++) {
      minWithinSubarray = Math.min(minWithinSubarray, arr[i]);
      maxWithinSubarray = Math.max(maxWithinSubarray, arr[i]);
  }
  
  // Expand the subarray to include elements that are smaller than minWithinSubarray
  while (left > 0 && arr[left - 1] > minWithinSubarray) {
      left--;
  }
  
  // Expand the subarray to include elements that are larger than maxWithinSubarray
  while (right < n - 1 && arr[right + 1] < maxWithinSubarray) {
      right++;
  }
  
  // The length of the shortest subarray that needs to be sorted
  return right - left + 1;
}

const arr = [2, 6, 4, 8, 10, 9, 15];
const lengthOfSubarray = findUnsortedSubarray(arr);
console.log("Length of the shortest subarray to be sorted:", lengthOfSubarray);
//=====================================================================================================================================================
//=====================================================================================================================================================

// Q.> Longest Subarray with Equal Elements: Find the length of the longest subarray within an array where all elements are the same.
function longestSubarrayWithEqualElements(arr) {
  let maxLength = 1; // At least one element will be present
  let currentLength = 1;

  for (let i = 1; i < arr.length; i++) {
      if (arr[i] === arr[i - 1]) {
          currentLength++;
          maxLength = Math.max(maxLength, currentLength);
      } else {
          currentLength = 1;
      }
  }

  return maxLength;
}

const arr = [2, 2, 3, 3, 3, 4, 4, 4, 4, 4];
const lengthOfLongestSubarray = longestSubarrayWithEqualElements(arr);

console.log(`Length of the longest subarray with equal elements: ${lengthOfLongestSubarray}`);


// To find a subarray with a sum of zero in a given array using JavaScript, you
//  can use a similar approach to the one used in the "Maximum Length of Subarray with Sum Zero" problem. 

function findSubarrayWithSumZero(arr) {
    const sumToIndex = new Map();
    let sum = 0;
    let startIndex = 0;

    for (let i = 0; i < arr.length; i++) {
        sum += arr[i];

        if (sum === 0) {
            // If the current sum is zero, the subarray from the beginning to the current index has a sum of zero.
            return arr.slice(startIndex, i + 1);
        }

        if (sumToIndex.has(sum)) {
            // If the same sum has been seen before, it means a subarray with a sum of zero exists.
            startIndex = sumToIndex.get(sum) + 1;
            return arr.slice(startIndex, i + 1);
        } else {
            // Store the first occurrence of the sum along with its index.
            sumToIndex.set(sum, i);
        }
    }

    // If no subarray with a sum of zero is found, return an empty array.
    return [];
}

const arr = [1, 2, -2, 4, -4];
const subarray = findSubarrayWithSumZero(arr);
console.log(subarray); // Output: [2, -2]